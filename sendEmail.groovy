/******************************************************************************************
ENVIAR MAIL 
******************************************************************************************/

import com.atlassian.mail.Email
import com.atlassian.mail.server.MailServerManager
import com.atlassian.mail.server.SMTPMailServer
import com.atlassian.jira.component.ComponentAccessor
//import com.atlassian.jira.functest.framework.admin

def subject = "test"
def body = "test"
//def emailAddr = issue.getReporter().getEmailAddress()
def emailAddr = "nicolas.carrivale@tsoftlatam.com"
 
def sendEmail(emailAddr, subject, body)
{
def mailServer = ComponentAccessor.getMailServerManager().getDefaultSMTPMailServer()
    
    if (mailServer) 
    {
        Email email = new Email(emailAddr);
        email.setSubject(subject);
        email.setBody(body);
        mailServer.send(email);
	}
    
    else
    {
        // Problem getting the mail server from JIRA configuration, log this error
    }
}

sendEmail (emailAddr, subject, body)