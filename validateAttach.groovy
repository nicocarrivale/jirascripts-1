/******************************************************************************************
VALIDACION - AL MENOS UN ATTACH
*******************************************************************************************/

import com.atlassian.jira.component.ComponentAccessor
ComponentAccessor.attachmentManager.getAttachments(issue).size() >= 1