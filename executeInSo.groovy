
/******************************************************************************************
EJECUCION DE COMANDO EN EL SO, DESDE JIRA
******************************************************************************************/

import com.atlassian.jira.ComponentManager
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.MutableIssue
import com.atlassian.jira.issue.comments.CommentManager
import com.atlassian.jira.issue.attachment.Attachment
import org.apache.log4j.Category
import com.atlassian.jira.issue.Issue

compManager = ComponentManager.getInstance()
// Traigo el nombre de user
def currentUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
def user = currentUser.getName()

// Traigo objetos y luego su key (caso y del proyecto)
//MutableIssue issuekey = ComponentAccessor.getIssueManager().getIssueObject('SJ-57')
def idissue = issue.getKey()
def proj= issue.getProjectObject() 
def keyproj = proj.getKey()


// traigo Manejador de adjuntos
def attachmentManager = ComponentAccessor.getAttachmentManager()

// Traigo lista de adjuntos
def attachlist = attachmentManager.getAttachments(issuekey)

// Tomo el adjunto en primer lugar y su nombre 
def attachid= attachlist[0].getId()
def nombre= attachmentManager.getAttachment(attachid).getFilename()


// Llamada al SO para la copia
def sout = new StringBuilder(), serr = new StringBuilder()
def str= 'cp /opt/atlassian/application-data/jira/data/attachments/'+keyproj+'/'+idissue+'/'+attachid+' /tmp/'+nombre
def proc = str.execute()
proc.consumeProcessOutput(sout, serr)
proc.waitForOrKill(1000) 
def resp= 'Salida: \n'+sout+'\n\n'+serr

// Comento el resultado
CommentManager commentMgr = compManager.getCommentManager()
commentMgr = (CommentManager) compManager.getComponentInstanceOfType(CommentManager.class)
commentMgr.create(issue, user, resp, false)
