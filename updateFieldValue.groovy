/******************************************************************************************
UPDATEAR VALOR DE UN CAMPO
*******************************************************************************************/

import com.atlassian.jira.issue.Issue
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.ComponentManager
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.fields.CustomField
import com.atlassian.jira.issue.ModifiedValue
import com.atlassian.jira.issue.util.DefaultIssueChangeHolder

//Identify Issue
def im = ComponentAccessor.getIssueManager()
def issue = im.getIssueObject("PHP-13")
def cfm = ComponentAccessor.getCustomFieldManager()

//Set Change Holder
def changeHolder = new DefaultIssueChangeHolder();

//Set Value of Manufactuer
def cf1 = cfm.getCustomFieldObjects(issue).find {it.name == "Valor"}
cf1.updateValue(null, issue, new ModifiedValue(issue.getCustomFieldValue(cf1), 'TEST_VALUE'), changeHolder);