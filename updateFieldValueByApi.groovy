/******************************************************************************************
UPDATEAR VALOR DE CAMPOS POR API REST
*******************************************************************************************/

def issueKey = issue.key
// get custom fields
def customFields = get("/rest/api/2/field")
    .asObject(List)
    .body
    .findAll { (it as Map).custom } as List<Map>

// Tomo el issue
def issue = get("/rest/api/2/issue/${issueKey}") //${issueKey}
    .header('Content-Type', 'application/json')
    .asObject(Map)
    .body

//Obtengo todos los ids de los campos
def orgAplicCf = customFields.find { it.name == 'Organismo de aplicacion' }?.id
def dirOmicCf = customFields.find { it.name == 'Direccion OMIC' }?.id
def contactoOrgCf = customFields.find { it.name == 'Contacto Org.' }?.id
def mailOrgCf = customFields.find { it.name == 'Email Org.' }?.id
def telOrgCf = customFields.find { it.name == 'Telefono Org.' }?.id
def CoprecCf = customFields.find { it.name == 'COPREC' }?.id

// Get all the fields from the issue as a Map
def fields = issue.fields as Map

def provinciaCiudadCf = customFields.find { it.name == 'Provincia / Ciudad' }?.id
def valueCfProvincia = fields[provinciaCiudadCf] as Map //Con este valor haremos el if para calcular el estudio
def valueCoprecCf = fields[CoprecCf] as Map

// Get each of the values from the multi select list field and store them
def ciudadSeleccionadaMap = valueCfProvincia?.child as Map
def ciudadSeleccionada = ciudadSeleccionadaMap?.value
def provinciaSeleccionada = valueCfProvincia?.value
def coprecSeleccionadaMap = valueCoprecCf?.child as Map
def COPREC = coprecSeleccionadaMap?.value

def orgAplicValue = ""
def dirOmicValue = ""
def contactoOrgValue = ""
def mailOrgValue = ""
def telOrgValue = ""

// Descarto OMIC si COPREC = Si y Provincia = "Capital Federal" (VALIDAR *********)

if (COPREC != "No" && provinciaSeleccionada != "Capital Federal") {

//En base a la provincia defino la Omic
// Casos por Provincia y dentro Casos por Ciudad
// OJO CABA que tienen todos GCP tienen CP 1000 **************************

    switch(provinciaSeleccionada){
          case "Catamarca":
            switch(ciudadSeleccionada){
                case "San Fernando Del Valle De Catamarca (4700)":
                    orgAplicValue = "Defensa del Consumidor"
                    dirOmicValue = "Avenida General Belgrano Nro 1494-Pabellón 27"
                    contactoOrgValue = ""
                    telOrgValue = ""
                    mailOrgValue = ""
                    break
                case "San Jose (Dpto. Santa Maria) (4139)":
                    orgAplicValue = "Oficina de Defensa al Consumidor Munic. San Jose"
                    dirOmicValue = "Calle 16 de Agosto s/n"
                    contactoOrgValue = ""
                    telOrgValue = ""
                    mailOrgValue = ""
                    break
                default:
                    orgAplicValue = ""
                    break
            }

case "Chaco":
            switch(ciudadSeleccionada){
                case "Barranqueras (3503)":
                    orgAplicValue = "Municipalidad de Barranqueras- Area Defensa del Consumidor"
                    dirOmicValue = "Avenida Laprida 5601"
                    contactoOrgValue = ""
                    telOrgValue = "0362-4581058"
                    mailOrgValue = "defensadelconsumidor@barranqueras.gob.ar"
                    break
                case "Fontana (3514)":
                    orgAplicValue = "Ministerio de Industria, Empleo y Trabajo - Secretaria de Comercio"
                    dirOmicValue = "9 de Julio"
                    contactoOrgValue = ""
                    telOrgValue = "3624475857"
                    mailOrgValue = ""
                    break
                default:
                    orgAplicValue = ""
                    break
            }

}
} // cierro el IF COPREC

//Actualizo los campos
put("/rest/api/2/issue/${issueKey}") //
    .queryString("overrideScreenSecurity", Boolean.TRUE)
    .header("Content-Type", "application/json")
    .body([
    fields:[
        (orgAplicCf): orgAplicValue,
        (dirOmicCf): dirOmicValue,
        (contactoOrgCf): contactoOrgValue,
        (mailOrgCf): mailOrgValue,
        (telOrgCf): telOrgValue,
    ]
]).asString()